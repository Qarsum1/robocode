package nl.saxion;
import robocode.HitWallEvent;
import robocode.WinEvent;

import robocode.*;
import java.awt.*;
import java.io.IOException;
import sampleteam.Point;


import static robocode.util.Utils.normalRelativeAngleDegrees;


public class Hunter extends TeamRobot {
    int moveDirection = 1;//which way to move
    String[] teammates = getTeammates();

    /**
     * run:  Tracker's main run function
     */
    public void run() {
        setAdjustRadarForRobotTurn(true);//keep the radar still while we turn
        setBodyColor(new Color(128, 128, 50));
        setGunColor(new Color(50, 50, 20));
        setRadarColor(new Color(200, 200, 70));
        setScanColor(Color.white);
        setBulletColor(Color.blue);
        setAdjustGunForRobotTurn(true); // Keep the gun still when we turn
        turnRadarRightRadians(Double.POSITIVE_INFINITY);//keep turning radar right

        String[] teammates = getTeammates();
        if (teammates != null) {
            for (String member : teammates) {
                out.println(member);
            }
        }
        try {
            broadcastMessage(teammates);
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        @Override
        public void onMessageReceived (MessageEvent e){
            Object msg = e.getMessage();
            if (msg instanceof Point) {
                Point p = (Point) e.getMessage();
                // Calculate x and y to target
                double dx = p.getX() - this.getX();
                double dy = p.getY() - this.getY();
                System.out.println(" point is " + dx + "and " + dy);
                // Calculate angle to target
//            double theta = Math.toDegrees(Math.atan2(dx, dy));
//
//            // Turn gun to target
//            turnGunRight(normalRelativeAngleDegrees(theta - getGunHeading()));
                // Fire hard!
                fire(3);
            }

        }


        /**
         * onScannedRobot:  Here's the good stuff
         */
        public void onScannedRobot (ScannedRobotEvent e){
            if (isTeammate(e.getName())) {
                System.out.println(" is my team.cant kill");
                return;
            } else {

             try {
                 findTeammate("sniper");
                     // Send enemy position to teammates
                 { broadcastMessage(new Point(getX(), getY()));


                  }



                 out.println("message sent: ");

               }
                catch (IOException ex) {
                    out.println("Unable to send order: ");
                    ex.printStackTrace(out);
                }


                double absBearing = e.getBearingRadians() + getHeadingRadians();//enemies absolute bearing
                double latVel = e.getVelocity() * Math.sin(e.getHeadingRadians() - absBearing);//enemies later velocity
                double gunTurnAmt;//amount to turn our gun
                setTurnRadarLeftRadians(getRadarTurnRemainingRadians());//lock on the radar
                if (Math.random() > .9) {
                    setMaxVelocity((12 * Math.random()) + 12);//randomly change speed
                }
                if (e.getDistance() > 150) {//if distance is greater than 150
                    gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 22);//amount to turn our gun, lead just a little bit
                    setTurnGunRightRadians(gunTurnAmt); //turn our gun
                    setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getHeadingRadians() + latVel / getVelocity()));//drive towards the enemies predicted future location
                    setAhead((e.getDistance() - 140) * moveDirection);//move forward
                    setFire(3);//fire
                } else {//if we are close enough...
                    gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 15);//amount to turn our gun, lead just a little bit
                    setTurnGunRightRadians(gunTurnAmt);//turn our gun
                    setTurnLeft(-90 - e.getBearing()); //turn perpendicular to the enemy
                    setAhead((e.getDistance() - 140) * moveDirection);//move forward
                    setFire(3);//fire
                }

            }
        }




    public void onHitWall (HitWallEvent e){
            moveDirection = -moveDirection;//reverse direction upon hitting a wall
        }




        /**
         * onWin:  Do a victory dance
         */
        public void onWin (WinEvent e){
            for (int i = 0; i < 50; i++) {
                turnRight(30);
                turnLeft(30);
            }
        }

        public String[] getTeammates () {
            String[] names = {" Dummy", "Sniper", "Hunter", "Hunter", "Hunter"};
            return names;
        }
    private String findTeammate(String sniper) {
            String n="";
        for (String member : teammates) {
           if (member.equalsIgnoreCase("sniper")){
               n=""+member;
            }
        }
        return n;
  }
    }


