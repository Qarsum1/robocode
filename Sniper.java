package nl.saxion;
import robocode.*;
import robocode.util.Utils;
import sampleteam.Point;
import java.awt.*;
import java.io.IOException;


import static robocode.util.Utils.normalRelativeAngleDegrees;

public class Sniper extends TeamRobot {



    // bullet power.
    private static double maxBP=3;


    // direction
    private int dir = 1;
    static int corner = 0;
    //Enemy's X and Y.
    private static double EX;
    static double EY;


    public void run() {
        // Set colors for robots.
        setRobotColors();
        turnRight(normalRelativeAngleDegrees(corner - getHeading()));

        // Move to that wall
        ahead(5000);
        // Turn to face the corner
        turnLeft(90);
        //Setting radar and gun independent of robot movement.
        setAdjustGunForRobotTurn(true);
        setAdjustRadarForGunTurn(true);
        while (true) {
            turnRadarRightRadians(Double.POSITIVE_INFINITY);
        }
    }
    /**
     * onMessageReceived:  move to the position sent and fight..
     * @Param  message event named as ...e
     */
        @Override
        public void onMessageReceived(MessageEvent e) {
            Object msg = e.getMessage();
            if (msg instanceof Point) {
                Point p = (Point) e.getMessage();
                // Calculate x and y to target
                double dx = p.getX() - this.getX();
                double dy = p.getY() - this.getY();
                System.out.println(" point is "+dx + "and " + dy);
                // Calculate angle to target
                double theta = Math.toDegrees(Math.atan2(dx, dy));
                // Turn gun to target
                turnGunRight(normalRelativeAngleDegrees(theta - getGunHeading()));
                // Fire hard!
                fire(3);}

    }
    /**
     * onScannedRobot:  fire if its not my team mate.
     * @Param otherScannedRobot event.
     */

    public void onScannedRobot(ScannedRobotEvent event) {
        // check the other robots' name if its from my team do nothing.
        if (isTeammate(event.getName())) {
            System.out.println("is my team.cant kill");
        } // check the other robots' name if its from opponents fire at them.
        else {//Our maximum bulletPower is 3, unless we are further than 200 distance units away from them
            //, if distance is more than 200, bullet power is 2.4.
            if (event.getDistance() > 200) {
                maxBP = 2.4;
            }//determine bullet power using Math function.
            double bulletPower = Math.min(maxBP, Math.min(getEnergy() / 10, event.getEnergy() / 4));
            //Find our enemy's absolute bearing.
            double absBearing = event.getBearingRadians() + getHeadingRadians();
            //This finds our enemies X and Y positions.
            EX = getX() + event.getDistance() * Math.sin(absBearing);
            EY = getY() + event.getDistance() * Math.cos(absBearing);

            double predictedX = EX, predictedY = EY;
            //Find the bearing of our predicted coordinates from us.
            double aim = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));
            //Aim and fire.
            setTurnGunRightRadians(Utils.normalRelativeAngle(aim - getGunHeadingRadians()));
            setFire(bulletPower);
            setAhead(100 * dir);
            setTurnRadarRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getRadarHeadingRadians()) * 2);
        }
    }
    /**
     * onBulletHit: move Away.
     * @param    event
     */
    public void onBulletHit(BulletHitEvent event) {
        turnRight(normalRelativeAngleDegrees(90 - (getHeading())));
        ahead(dir);
    }
    /**
     *
     * setting colours for robots and its parts
     */

    private void setRobotColors(){
        setBodyColor(Color.YELLOW);
        setGunColor(Color.black);
        setRadarColor(Color.orange);
        setBulletColor(Color.cyan);
        setScanColor(Color.cyan);
    }
}


