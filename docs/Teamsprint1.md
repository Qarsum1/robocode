# Project Documentation
___
**Team Members**
* Rhutik
* Qarsum
* Sven
* Kaloyan
* Michael

<p> The team tried to meet two times a week other than the scheduled classes to discuss the strategies and to share the ideas with each other. Group tried to distribute the work equally among all members to avoid the workload on a single person and to maximize our learning through this project.</p>

**Sprint Objective**
<p> To create a team of advanced robots consisting of five bots. The purpose is to communicate with each other through messages broadcasted by the leader to all four members and to avoid friendly fire while working as a team to battle against the opponents. </p>

>This has been achieved in this sprint and the same is uploaded in the repository.


### List of the task
**Creating Robots**
- Kaloyan
- Michael
- Qarsum

<p> Team member Qarsum and Michal worked on building the team robots. Michael came up with an idea of making a robot that will actually track the opponents on the battlefield and we made three robots of this kind whereas Qarsum made two robots, one with a linear movement along the walls whereas the other one behaved like something similar to the corner robots. Also, Qarsum figured out how messages can be sent through the leader of the team to the other team members and how to avoid friendly fire between each other. Unfortunately, Kaloyan was not able to join us till the day before the submission but he actively took part in the project when he showed up and helped Qarsum to sort out some problems with the linear robots and to upload the team robots in the battle server with her.
</p>

**Documentation:**
* Rhutik _(Markdown syntax)_
* Qarsum _(Technical Documentation)_

<p> In the next sprint we plan to exchange our positions, this way we would be able to work on all the things together.</p>

**Team Scrum Retrospective**
<p>
The first obligatory requirements of this sprint have been achieved which was basic communication between the team and avoidance of friendly fire. The good part of the sprint was that by the end of the sprint, we were able to build the robots and to upload them in the battle server which did not seem possible due to the poor team coordination and absence of a majority of members due to the health concerns. Secondly, the scanning and movement of one of the linear robots have not worked out properly due to the time constraints which would be resolved in the next phase.
In the next phase, we will try to keep track of the performance of all the members to work as a team altogether. Finalizing, the team members have decided to be there in all the group discussions during the next sprint and to put their maximum efforts to overcome the problems faced during this sprint so we can prepare and create better outputs.
 </p>


# Technical Documentation
---
**Analysis of the previous sprints:**
<p>Truly speaking none of our group members have uploaded their individual robots in the battle server so we can not actually analyze and come up with the suggestions by comparing the results. Looking at the battle results, our team performed quite well and we will try to improve the weak points and to focus on advanced communication between crew members as well as robots on the battlefield.
</p>

**Strategy description:**
<p> The idea behind the team of robots is to create five tank members, three of which will go on the battlefield to track the enemies, trying to avoid bullets on their way, if close enough, to encircle the target or to stand in a better position than the opponent. The other two of them will be having linear or corner movements to control the enemies on the boundaries of the field. There will be one leader of the team who will keep track of all the team members and try to protect the one in danger by sending the position of the enemies to the other members. The rest of the team members will try to protect him and the one who is near to it will move to the position. In addition to that, if the leader dies, leadership will be passed on to the next member in the team. That could be achieved by having a method dedicated for this purpose.
</p>

**Friendly fire avoidance**

<p> his is achieved by creating a String array with the names of all the team members, the leader broadcasted the message to all the members (teammates). Each of the members has a method in the “onScannedRobot” method, that they will fire only if the name is not in their team lists broadcasted by the team leader. </p>
