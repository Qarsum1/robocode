package nl.saxion;

import robocode.*;
import robocode.util.Utils;
import sampleteam.Point;

import java.awt.*;

import static robocode.util.Utils.normalRelativeAngleDegrees;

public class Dummy extends TeamRobot {
    private int dir = 1;
    private static double EX;
    private static double EY;
    private double maxBP = 3;

    public void run() {
        //method for setting colors for robots.
        setRobotColors();
        setTurnRadarRight(Double.POSITIVE_INFINITY);
        // turnLeft to face a wall.
        // getHeading() % 90 means the remainder of
        // getHeading() divided by 90.
        turnLeft(getHeading() % 90);
        while (true) {
            if (Utils.isNear(getHeadingRadians(), 0D) || Utils.isNear(getHeadingRadians(), Math.PI)) {
                ahead((Math.max(getBattleFieldHeight() - getY(), getY()) - 28) * dir);
            } else {
                ahead((Math.max(getBattleFieldWidth() - getX(), getX()) - 28) * dir);
            }
            // Turn to the next wall
            turnRight(90);
            execute();
        }
    }

    /**
     * onScannedRobot:  fire if its not my team mate.
     * @Param other ScannedRobot event.
     */

    public void onScannedRobot(ScannedRobotEvent e) {
        // check the other robots' name if its from my team do nothing.
        if (isTeammate(e.getName())) {
            System.out.println("hey team mate!");
        } else// check the other robots' name if its from opponents fire at them.
        {
            if (e.getDistance() > 200) {
                maxBP = 2.4;
            } else {
                // calculates bullet power based on the energy levels.
                double bulletPower = Math.min(maxBP, Math.min(getEnergy()/ 10,e.getEnergy()/ 4));
                //Find our enemy's absolute bearing .
                double absBearing = e.getBearingRadians() + getHeadingRadians();

                //This finds our enemies X and Y
                EX = getX() + e.getDistance() * Math.sin(absBearing);
                EY = getY() + e.getDistance() * Math.cos(absBearing);
                double predictedX = EX, predictedY = EY;
                //Find the bearing of our predicted coordinates from us.
                double aim = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));
                //Aim and fire.
                setTurnGunRightRadians(Utils.normalRelativeAngle(aim - getGunHeadingRadians()));
                setFire(bulletPower);
                setAhead(100 * dir);
                setTurnRadarRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getRadarHeadingRadians()) * 2);
            }
        }
    }
    /**
     * onBulletHit: move Away.
     * @param    event
     */

    public void onBulletHit(BulletHitEvent event) {
        turnRight(normalRelativeAngleDegrees(90 - (getHeading())));
        ahead(dir);//The robot goes away from the enemy.
    }

    /**
     * onMessageReceived:  move to the position sent and fight..
     * @Param  message event named as ...e
     */

    public void onMessageReceived(MessageEvent e) {
        // Fire at a point
        if (e.getMessage() instanceof Point) {
            Point p = (Point) e.getMessage();
            // Calculate x and y to target
            double dx = p.getX() - this.getX();
            double dy = p.getY() - this.getY();
            System.out.println(" point is " + dx + "and " + dy);
            // Calculate angle to target
            double theta = Math.toDegrees(Math.atan2(dx, dy));
            // Turn gun to target
            setTurnGunRight(normalRelativeAngleDegrees(theta - getGunHeading()));
        }
    }
    /**
     *setting colours for robots and its parts
     */
    private void setRobotColors(){
        setBodyColor(Color.YELLOW);
        setGunColor(Color.black);
        setRadarColor(Color.orange);
        setBulletColor(Color.cyan);
        setScanColor(Color.cyan);

    }
}




